from django.views.generic.list import ListView
from django.utils import timezone
from .models import Userfiles
from .forms import DataForm
from TweetAnalyzer.tweets import Analyze
# Create your views here.
from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth import get_user,authenticate,login,get_user_model,logout
from .forms import LoginForm,RegisterForm
from django.contrib.auth.decorators import login_required

def home(request):
    #print(get_user(request).username)
    return render(request,'home.html')

@login_required(login_url='/login/')
def analyze(request):
    user = get_user(request)
    data_form = DataForm(request.POST or None)
    context = {
        "analysis_form": data_form
    }
    if data_form.is_valid():
        Tweet_count=int(data_form.cleaned_data.get("Tweet_Count"))
        Topic=data_form.cleaned_data.get("Topic")
        print(Tweet_count)
        print(Topic)
        p=str(Analyze(Tweet_count,Topic,user))
        context['html_content']=p
        context['title']='Welocome to Tweet Analyzer'
        return render(request,'Analysis.html',context)
    return render(request,'Analysis.html',context)


def login_page(request):
    form=LoginForm(request.POST or None)
    context = {
        "form": form
    }

    user=get_user(request)

    if form.is_valid():
        print(form.cleaned_data)
        username=form.cleaned_data.get('username')
        password=form.cleaned_data.get('password')
        user=authenticate(request,username=username,password=password)
        print(type(user.username))
        print(request.user.is_authenticated)
        if user is not None:
            print(request.user.is_authenticated)
            login(request,user)
            #context['form'] = LoginForm()
            return redirect('/')
        else:
            context['form'] = LoginForm()
            context['errmsg']="Invalid Credentials"
    return render(request,'auth/login.html',context)


def register_page(request):
    User = get_user_model()
    form = RegisterForm(request.POST or None)
    context = {
        "form": form
    }
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        new_user=User.objects.create_user(username,email,password)
        print(new_user)
        context['msg']='Successfully Registered !! Go to Login'
    return render(request,'auth/register.html',context)

@login_required(login_url='/login/')
def logout_view(request):
    logout(request)
    return redirect('/')


class UserfilesListView(ListView):
    model = Userfiles
    paginate_by = 10
    template_name = 'myfiles.html'
    def get_queryset(self):
        request=self.request
        print(get_user(request).username)
        #print(Userfiles.objects.filter(username__icontains=str(get_user(request).username)))
        return Userfiles.objects.filter(username__icontains=str(get_user(request).username))
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['filelist']=self.get_queryset()
        return context


@login_required(login_url='/login/')
def removefile(request):
    file_id = request.POST.get('file_id')
    if file_id is not None:
        instance=Userfiles.objects.get(id=file_id)
        if instance is not None:
            instance.remove_file(instance.id)
            return redirect('myfiles')
    return redirect('myfiles')

@login_required(login_url='/login/')
def viewfile(request):
    file_id = request.POST.get('file_id')
    if file_id is not None:
        instance=Userfiles.objects.get(id=file_id)
        if instance is not None:
            contents=instance.view_file(instance.id)
            return render(request,'results.html',contents)
    return redirect('myfiles')
