import requests
from requests.auth import HTTPBasicAuth
import plotly
import plotly.plotly as py
from django.db import models
from django.contrib.auth.models import User
import os
import pandas as pd
headers = {'Plotly-Client-Platform': 'python'}
us = 'jainrj100'
api_key = '3GyJTIGmhoOSywqMeERP'
auth=HTTPBasicAuth(us,api_key)

class Userfiles(models.Model):
    username=models.CharField(blank=False,null=False,max_length=250,default='rahuljain')
    filename=models.CharField(blank=True,null=True,max_length=250)
    modified_name=models.CharField(blank=True,null=True,max_length=250)
    filepath=models.CharField(blank=True,null=True,max_length=250)
    graph=models.CharField(blank=True,null=True,max_length=250)
    graph_url=models.CharField(blank=True,null=True,max_length=250)

    def __str__(self):
        return self.username +'/'+self.filename

    def remove_file(self,fileid=None):
        qs = Userfiles.objects.filter(id=fileid)
        if qs.count()==1:
            filepath=qs.first().filepath
            url=qs.first().graph_url
            instance = Userfiles.objects.get(id=fileid)
            try:
                plotly.tools.set_credentials_file(username=us, api_key=api_key)
                uid=url.split('/')
                fid = us + ":" + uid[-1]
                a=requests.post('https://api.plot.ly/v2/files/' + fid + '/trash', auth=auth, headers=headers)
                os.remove(filepath)
                instance.delete()
                print('succesfully deleted')
            except:
                print('error')

    def view_file(self,fileid=None):
        qs = Userfiles.objects.filter(id=fileid)
        if qs.count()==1:
            filepath=qs.first().filepath
            graph=qs.first().graph
            df=pd.read_csv(filepath)
            html_content=df.to_html(max_rows=None,max_cols=None,classes='table',escape=False,bold_rows=True,justify='center',border=2)
            contents={'graph':graph,'html_content':html_content}
            return contents


