# Generated by Django 2.0.6 on 2018-06-24 08:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_userfiles_modified_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userfiles',
            name='user',
        ),
        migrations.AddField(
            model_name='userfiles',
            name='username',
            field=models.CharField(default='rahuljain', max_length=250),
        ),
    ]
