from tweepy import Stream
import tweepy
import random
from tweepy import OAuthHandler
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.tools as tls
from django.core.files.storage import FileSystemStorage
from tweepy.streaming import StreamListener
import json
import sys,os
import csv
from collections import Counter
from aylienapiclient import textapi
import matplotlib.pyplot as plt
from .settings import MEDIA_ROOT
from account.models import Userfiles


def Analyze(count,topic,user=None):

    # Twitter credentials
    ckey="h7ytI9UlARU1VrlxuvLWhhSSS"
    csecret="J9qAu5lw1hu5U5I7vdhryinLtTGTA7C68tLvnXvVALUxV0zMGa"
    atoken="733277923147407360-u5TgOQ1cAwJa0DsoNmmNKJlBwRGkmoo"
    asecret="l7ZdN0wDvUzt4iDjPFfYQXmkuuHgLev7dFQ1phUhlmxsl"

    # AYLIEN credentials
    application_id = "3fb2faaf"
    application_key = "c95cd662b0f326df5e16c37f922ffd50"

    ## set up an instance of Tweepy
    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)
    api = tweepy.API(auth)

    ## set up an instance of the AYLIEN Text API
    client = textapi.Client(application_id, application_key)

    ## search Twitter for something that interests you
    query = topic
    number = count

    results = api.search(
       lang="en",
       q=query + " -rt",
       count=number,
       result_type="recent"
    )

    print("--- Gathered Tweets \n")

    ## open a csv file to store the Tweets and their sentiment
    original_name='Sentiment_Analysis_of_{}_Tweets_About_{}.csv'.format(number, query)
    file_name = 'Sentiment_Analysis_of_{}_Tweets_About_{}_{}.csv'.format(number, query,random.randint(1,10000))
    modified_name=file_name
    file_name=os.path.join(MEDIA_ROOT,file_name)
    with open(file_name, 'w', newline='') as csvfile:
       csv_writer = csv.DictWriter(
           f=csvfile,
           fieldnames=["Tweet", "Sentiment"]
       )
       csv_writer.writeheader()

       print("--- Opened a CSV file to store the results of your sentiment analysis... \n")


    ## tidy up the Tweets and send each to the AYLIEN Text API
       for c, result in enumerate(results, start=1):
           tweet = result.text
           tidy_tweet = tweet.strip().encode('ascii', 'ignore')

           if len(tweet) == 0:
               print('Empty Tweet')
               continue

           response = client.Sentiment({'text': tidy_tweet})
           csv_writer.writerow({
               'Tweet': response['text'],
               'Sentiment': response['polarity']
           })

           print("Analyzed Tweet {}".format(c))


    ## count the data in the Sentiment column of the CSV file
    with open(file_name, 'r') as data:
       counter = Counter()
       for row in csv.DictReader(data):
           counter[row['Sentiment']] += 1

       positive = counter['positive']
       negative = counter['negative']
       neutral = counter['neutral']

    ## declare the variables for the pie chart, using the Counter variables for "sizes"
    colors = ['green', 'red', 'grey']
    values = [positive, negative, neutral]
    labels = ['Positive', 'Negative', 'Neutral']

    username = 'jainrj100'
    api_key = '3GyJTIGmhoOSywqMeERP'
    plotly.tools.set_credentials_file(username, api_key)
    html_url=py.plot([go.Pie(labels=labels, values=values)],auto_open=False)
    html_content=tls.get_embed(html_url)

    file =Userfiles(username=user.username,filename=original_name,filepath=file_name,modified_name=modified_name,
                    graph=str(html_content),graph_url=str(html_url))
    file.save()
    #print(fs.base_url(file_name))
    #file.save()
    #os.remove(file_name)
    return html_content
