"""TweetAnalyzer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path
from account import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('^$',views.home,name='homepage'),
    path('analyze',views.analyze,name='analyze'),
re_path('^login/$',views.login_page,name='login' ),
re_path('^logout/$',views.logout_view,name='logout' ),
re_path('^register/$',views.register_page,name='register' ),
re_path('myfiles$',views.UserfilesListView.as_view(),name='myfiles'),
re_path('^remove',views.removefile,name='removefile'),
    re_path('^view',views.viewfile,name='viewfile'),


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
